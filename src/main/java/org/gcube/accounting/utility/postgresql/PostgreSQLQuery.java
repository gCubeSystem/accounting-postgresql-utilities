package org.gcube.accounting.utility.postgresql;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;

import org.gcube.documentstore.records.Record;
import org.postgresql.core.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostgreSQLQuery {

	protected static final Logger logger = LoggerFactory.getLogger(PostgreSQLQuery.class);
	
	private static final String UTC_TIME_ZONE = "UTC";
	
	public static final TimeZone DEFAULT_TIME_ZONE = TimeZone.getTimeZone(UTC_TIME_ZONE);
	
	public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS Z";
	
	protected StringBuffer stringBuffer;
	
	protected String getQuotedString(String string) throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("'");
		Utils.escapeLiteral(builder, string, false);
		builder.append("'");
		return builder.toString();
	}
	
	protected void appendString(String string) {
		stringBuffer.append("'");
		stringBuffer.append(string);
		stringBuffer.append("'");
	}
	
	protected void appendValue(Serializable serializable) throws SQLException {
		stringBuffer.append(getValue(serializable));
	}
	
	protected String getValue(Serializable serializable) throws SQLException {
		if(serializable instanceof Number) {
			return serializable.toString();
		}
		
		if(serializable instanceof Calendar) {
			Calendar calendar = (Calendar) serializable;
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATETIME_PATTERN);
			simpleDateFormat.setTimeZone(DEFAULT_TIME_ZONE);
			String date = simpleDateFormat.format(calendar.getTime());
			return getQuotedString(date);
		}
		
		if(serializable instanceof Enum) {
			Enum<?> e = (Enum<?>) serializable;
			return getQuotedString(e.name());
		}
		
		// String, URI etc
		return getQuotedString(serializable.toString());
	}
	
	public String getSQLInsertCommand(Record record) throws Exception {
		stringBuffer = new StringBuffer();
		RecordToDBFields recordToDBFields = RecordToDBMapping.getRecordToDBFields(record.getClass());
		
		stringBuffer.append("INSERT INTO ");
		stringBuffer.append(recordToDBFields.getTableName());
		
		boolean first = true;
		SortedSet<String> keys = new TreeSet<>(record.getRequiredFields());
		StringBuffer values = new StringBuffer();
		for(String key : keys) {
			if(first) {
				stringBuffer.append(" (");
				values.append(" (");
				first = false;
			}else {
				stringBuffer.append(",");
				values.append(",");
			}
			String dbField = recordToDBFields.getTableField(key);
			stringBuffer.append(dbField);
			switch (key) {
				case "creationTime": case "startTime": case "endTime":
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis((long) record.getResourceProperty(key));
					values.append(getValue(calendar));
					break;
	
				default:
					values.append(getValue(record.getResourceProperty(key)));	
					break;
			}		
		}
		stringBuffer.append(") VALUES");
		stringBuffer.append(values);
		stringBuffer.append(")");
		stringBuffer.append(" ON CONFLICT (");
		String dbField = recordToDBFields.getTableField(Record.ID);
		stringBuffer.append(dbField);
		stringBuffer.append(") DO NOTHING;");
		String ret = stringBuffer.toString();
		logger.trace("Created query {}", ret);
		return ret;
	}
	
}

package org.gcube.accounting.utility.postgresql;

import java.util.HashMap;
import java.util.Map;

import org.gcube.documentstore.persistence.PersistenceBackendConfiguration;
import org.gcube.documentstore.records.AggregatedRecord;
import org.gcube.documentstore.records.Record;
import org.gcube.documentstore.records.RecordUtility;

public class RecordToDBMapping {
	
	public static String getRecordTypeByClass(Class<? extends Record> clz) {
		try {
			Record r = clz.newInstance();
			return r.getRecordType();
		}catch (Exception e) {
			String type = clz.getSimpleName();
			type = type.replace("Abstract", "");
			type = type.replace("Aggregated", "");
			return type;
		}
	}
	
	protected static final Map<String, RecordToDBFields> classToRecordToDBMapper;
	
	static {
		classToRecordToDBMapper = new HashMap<>();
	}
	
	protected final Map<String, RecordToDBConnection> recordToDBInfo;
	
	public RecordToDBMapping() {
		recordToDBInfo=  new HashMap<>();
	}
	
	public void addRecordToDB(Class<? extends AggregatedRecord<?, ?>> clz, PersistenceBackendConfiguration configuration) throws Exception {
		String type = getRecordTypeByClass(clz);
		getRecordToDBFields(clz);
		RecordToDBConnection recordDBInfo = new RecordToDBConnection(type, clz, configuration);
		recordToDBInfo.put(type, recordDBInfo);
	}
	
	@SuppressWarnings("unchecked")
	public static synchronized RecordToDBFields getRecordToDBFields(Class<? extends Record> clz) throws Exception {
		String type = getRecordTypeByClass(clz);
		RecordToDBFields recordToDBFields = classToRecordToDBMapper.get(type);
		if(recordToDBFields == null) {
			Class<? extends AggregatedRecord<?,?>> aggregatedRecordClass;
			if(AggregatedRecord.class.isAssignableFrom(clz)) {
				aggregatedRecordClass = (Class<? extends AggregatedRecord<?, ?>>) clz;
			}else {
				aggregatedRecordClass = RecordUtility.getAggregatedRecordClass(type);
			}
			recordToDBFields = new RecordToDBFields(type, aggregatedRecordClass);
			classToRecordToDBMapper.put(type, recordToDBFields);
		}
		return recordToDBFields;
	}
	
	public synchronized RecordToDBConnection getRecordDBInfo(Class<? extends Record> clz) throws Exception {
		String type = getRecordTypeByClass(clz);
		return recordToDBInfo.get(type);
	}
	
}

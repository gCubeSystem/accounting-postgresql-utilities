package org.gcube.accounting.utility.postgresql;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.gcube.documentstore.records.Record;

public class RecordToDBFields {
	
	public static String getKey(String fieldName) {
		StringBuffer stringBuffer = new StringBuffer();
		int lenght = fieldName.length();
		boolean lastLowerCase = false;
        for (int i=0; i<lenght; i++) { 
           Character ch = fieldName.charAt(i); /*traversing String one by one*/
            if (Character.isUpperCase(ch)) {
            	if(lastLowerCase) {
            		stringBuffer.append("_");
            	}
            	lastLowerCase = false;
            }else {
            	lastLowerCase = true;
            }
            stringBuffer.append(Character.toLowerCase(ch));
        }
        return stringBuffer.toString();
	}
	
	protected final Class<? extends Record> clz;
	
	protected final String typeName;
	protected final String tableName;
	
	protected final Map<String, String> tableFieldToRecordField;
	protected final Map<String, String> recordFieldToTableField;
		
	protected RecordToDBFields(String typeName, Class<? extends Record> clz) throws Exception {
		this.clz = clz;
		this.typeName = typeName;
		this.tableName = RecordToDBFields.getKey(typeName);
		this.tableFieldToRecordField = new HashMap<>();
		this.recordFieldToTableField = new HashMap<>();
		mapFields();
	}
	
	protected void mapFields() throws Exception {
		Set<String> requiredFields = clz.newInstance().getRequiredFields();
		for(String usageRecordField : requiredFields) {
			String dbField = getKey(usageRecordField);
			tableFieldToRecordField.put(dbField, usageRecordField);
			recordFieldToTableField.put(usageRecordField, dbField);
		}
	}
	
	public String getTableField(String recordField) {
		return recordFieldToTableField.get(recordField);
	}
	
	public String getRecordField(String tableField) {
		String ret = tableFieldToRecordField.get(tableField);
		if(ret==null && recordFieldToTableField.keySet().contains(tableField)) {
			ret = tableField;
		}
		return ret;
	}

	public String getTypeName() {
		return typeName;
	}

	public String getTableName() {
		return tableName;
	}
	
}

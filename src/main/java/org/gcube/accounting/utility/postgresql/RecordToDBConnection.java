package org.gcube.accounting.utility.postgresql;

import java.sql.Connection;
import java.sql.DriverManager;

import org.gcube.documentstore.persistence.PersistenceBackendConfiguration;
import org.gcube.documentstore.records.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RecordToDBConnection {
	
	private static final Logger logger = LoggerFactory.getLogger(RecordToDBConnection.class);
	
	public static final String URL_PROPERTY_KEY = "URL";
	public static final String DB_SUFFIX = "-db";
	public static final String USERNAME_SUFFIX = "-username";
	public static final String PASSWORD_SUFFIX = "-password";
	
	private final String url;
	private final String username;
	private final String password;
	
	protected final Class<? extends Record> clz;
	protected final String typeName;
	
	protected RecordToDBConnection(String type, Class<? extends Record> clz, PersistenceBackendConfiguration configuration) throws Exception {
		this.clz = clz;
		this.typeName = type;
		this.url = configuration.getProperty(typeName+DB_SUFFIX);
		this.username = configuration.getProperty(typeName+USERNAME_SUFFIX);
		this.password = configuration.getProperty(typeName+PASSWORD_SUFFIX);
	}

	public Connection getConnection() throws Exception {
		Class.forName("org.postgresql.Driver");
		Connection connection = DriverManager.getConnection(url, username, password);
		logger.trace("Database {} opened successfully", url);
		connection.setAutoCommit(false);
		return connection;
	}
	
}

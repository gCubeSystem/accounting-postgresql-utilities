-- CREATE TYPE operation_result AS ENUM ('SUCCESS', 'FAILED');
CREATE TABLE "portlet_usage_record"(
	id UUID NOT NULL PRIMARY KEY,
	consumer_id TEXT NOT NULL,
	creation_time TIMESTAMP WITH TIME ZONE NOT NULL,
	scope TEXT NOT NULL,
	operation_result operation_result NOT NULL,
	caller_qualifier TEXT NOT NULL DEFAULT 'TOKEN',
	portlet_id TEXT NOT NULL,
	operation_id TEXT NOT NULL,
	operation_count INTEGER NOT NULL DEFAULT 1,
	aggregated BOOLEAN NOT NULL DEFAULT true,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL
);
CREATE EXTENSION IF NOT EXISTS timescaledb;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TYPE aggregation_state AS ENUM ('RESTARTED', 'STARTED', 'AGGREGATED', 'DELETED', 'ADDED', 'COMPLETED');
CREATE TYPE aggregation_type AS ENUM ('DAILY', 'MONTHLY', 'YEARLY');

CREATE TABLE "aggregation_status"(
	id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4 (),
	record_type TEXT NOT NULL,
	aggregation_type aggregation_type NOT NULL,
	aggregation_start_date TIMESTAMP WITH TIME ZONE NOT NULL,
	aggregation_end_date TIMESTAMP WITH TIME ZONE NOT NULL,
	original_records_number NUMERIC NOT NULL,
	aggregated_records_number NUMERIC NOT NULL,
	recovered_records_number NUMERIC NOT NULL,
	malformed_records_number NUMERIC NOT NULL DEFAULT 0,
	percentage NUMERIC NOT NULL,
	restart_from NUMERIC NOT NULL DEFAULT 0,
	context TEXT,
	current_aggregation_state aggregation_state NOT NULL,
	last_update_time TIMESTAMP WITH TIME ZONE NOT NULL,
	previous UUID REFERENCES aggregation_status (id) DEFAULT NULL ON DELETE SET NULL
);

CREATE TABLE "aggregation_status_event"(
	aggregation_state aggregation_state NOT NULL,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL,
	aggregation_status UUID REFERENCES aggregation_status (id) ON DELETE CASCADE,
	UNIQUE (aggregation_state, start_time, end_time, aggregation_status)
);

CREATE TYPE operation_result AS ENUM ('SUCCESS', 'FAILED');

CREATE TABLE "job_usage_record"(
	id UUID NOT NULL PRIMARY KEY,
	consumer_id TEXT NOT NULL,
	creation_time TIMESTAMP WITH TIME ZONE NOT NULL,
	scope TEXT NOT NULL,
	operation_result operation_result NOT NULL,
	caller_qualifier TEXT NOT NULL DEFAULT 'TOKEN',
	host TEXT NOT NULL,
	service_class TEXT NOT NULL,
	service_name TEXT NOT NULL,
	job_name TEXT NOT NULL,
	duration NUMERIC NOT NULL,
	max_invocation_time NUMERIC NOT NULL,
	min_invocation_time NUMERIC NOT NULL,
	operation_count INTEGER NOT NULL DEFAULT 1,
	aggregated BOOLEAN NOT NULL DEFAULT true,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE "portlet_usage_record"(
	id UUID NOT NULL PRIMARY KEY,
	consumer_id TEXT NOT NULL,
	creation_time TIMESTAMP WITH TIME ZONE NOT NULL,
	scope TEXT NOT NULL,
	operation_result operation_result NOT NULL,
	caller_qualifier TEXT NOT NULL DEFAULT 'TOKEN',
	portlet_id TEXT NOT NULL,
	operation_id TEXT NOT NULL,
	operation_count INTEGER NOT NULL DEFAULT 1,
	aggregated BOOLEAN NOT NULL DEFAULT true,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE "service_usage_record"(
	id UUID NOT NULL PRIMARY KEY,
	consumer_id TEXT NOT NULL,
	creation_time TIMESTAMP WITH TIME ZONE NOT NULL,
	scope TEXT NOT NULL,
	operation_result operation_result NOT NULL,
	caller_qualifier TEXT NOT NULL DEFAULT 'TOKEN',
	caller_host TEXT NOT NULL,
	host TEXT NOT NULL,
	service_class TEXT NOT NULL,
	service_name TEXT NOT NULL,
	called_method TEXT NOT NULL,
	duration NUMERIC NOT NULL,
	max_invocation_time NUMERIC NOT NULL,
	min_invocation_time NUMERIC NOT NULL,
	operation_count INTEGER NOT NULL DEFAULT 1,
	aggregated BOOLEAN NOT NULL DEFAULT true,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TYPE data_type AS ENUM ('STORAGE', 'TREE', 'GEO', 'DATABASE', 'LOCAL', 'OTHER', 'JUPYTER', 'KUBERNETES');

CREATE TABLE "storage_status_record"(
	id UUID NOT NULL PRIMARY KEY,
	consumer_id TEXT NOT NULL,
	creation_time TIMESTAMP WITH TIME ZONE NOT NULL,
	scope TEXT NOT NULL,
	operation_result operation_result NOT NULL,
	data_type data_type NOT NULL,
	data_volume NUMERIC NOT NULL,
	data_count NUMERIC NOT NULL,
	provider_uri TEXT DEFAULT 'data.d4science.org',
	operation_count INTEGER NOT NULL DEFAULT 1,
	aggregated BOOLEAN NOT NULL DEFAULT true,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL,
	
	-- Deprecated and to be removed
	provider_id TEXT DEFAULT 'data.d4science.org',
	data_service_id TEXT DEFAULT 'NOT_NEEDED',
	data_service_class TEXT DEFAULT 'NOT_NEEDED',
	data_service_name TEXT DEFAULT 'NOT_NEEDED'
	
);

CREATE TYPE operation_type AS ENUM ('CREATE', 'READ', 'UPDATE', 'DELETE');

CREATE TABLE "storage_usage_record"(
	id UUID NOT NULL PRIMARY KEY,
	consumer_id TEXT NOT NULL,
	creation_time TIMESTAMP WITH TIME ZONE NOT NULL,
	scope TEXT NOT NULL,
	operation_result operation_result NOT NULL,
	resource_owner TEXT NOT NULL,
	provider_uri TEXT NOT NULL,
	operation_type operation_type NOT NULL,
	data_type data_type NOT NULL,
	data_volume NUMERIC NOT NULL,
	operation_count INTEGER NOT NULL DEFAULT 1,
	aggregated BOOLEAN NOT NULL DEFAULT true,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL,
	
	-- Deprecated and to be removed
	resource_scope TEXT DEFAULT 'NOT_NEEDED',
	resource_uri TEXT DEFAULT 'NOT_NEEDED'
	
);
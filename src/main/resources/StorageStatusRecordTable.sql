-- CREATE TYPE operation_result AS ENUM ('SUCCESS', 'FAILED');
-- CREATE TYPE data_type AS ENUM ('STORAGE', 'TREE', 'GEO', 'DATABASE', 'LOCAL', 'OTHER', 'JUPYTER', 'KUBERNETES');
CREATE TABLE "storage_status_record"(
	id UUID NOT NULL PRIMARY KEY,
	consumer_id TEXT NOT NULL,
	creation_time TIMESTAMP WITH TIME ZONE NOT NULL,
	scope TEXT NOT NULL,
	operation_result operation_result NOT NULL,
	data_type data_type NOT NULL,
	data_volume NUMERIC NOT NULL,
	data_count NUMERIC NOT NULL,
	provider_uri TEXT DEFAULT 'data.d4science.org',
	operation_count INTEGER NOT NULL DEFAULT 1,
	aggregated BOOLEAN NOT NULL DEFAULT true,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL,
	
	-- Deprecated and to be removed
	provider_id TEXT DEFAULT 'data.d4science.org',
	data_service_id TEXT DEFAULT 'NOT_NEEDED',
	data_service_class TEXT DEFAULT 'NOT_NEEDED',
	data_service_name TEXT DEFAULT 'NOT_NEEDED'
	
);
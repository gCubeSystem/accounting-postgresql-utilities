-- CREATE TYPE operation_result AS ENUM ('SUCCESS', 'FAILED');
CREATE TYPE operation_type AS ENUM ('CREATE', 'READ', 'UPDATE', 'DELETE');
CREATE TYPE data_type AS ENUM ('STORAGE', 'TREE', 'GEO', 'DATABASE', 'LOCAL', 'OTHER', 'JUPYTER', 'KUBERNETES');
CREATE TABLE "storage_usage_record"(
	id UUID NOT NULL PRIMARY KEY,
	consumer_id TEXT NOT NULL,
	creation_time TIMESTAMP WITH TIME ZONE NOT NULL,
	scope TEXT NOT NULL,
	operation_result operation_result NOT NULL,
	resource_owner TEXT NOT NULL,
	provider_uri TEXT NOT NULL,
	operation_type operation_type NOT NULL,
	data_type data_type NOT NULL,
	data_volume NUMERIC NOT NULL,
	operation_count INTEGER NOT NULL DEFAULT 1,
	aggregated BOOLEAN NOT NULL DEFAULT true,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL,
	
	-- Deprecated and to be removed
	resource_scope TEXT DEFAULT 'NOT_NEEDED',
	resource_uri TEXT DEFAULT 'NOT_NEEDED'
	
);
CREATE TYPE operation_result AS ENUM ('SUCCESS', 'FAILED');
CREATE TABLE "service_usage_record"(
	id UUID NOT NULL PRIMARY KEY,
	consumer_id TEXT NOT NULL,
	creation_time TIMESTAMP WITH TIME ZONE NOT NULL,
	scope TEXT NOT NULL,
	operation_result operation_result NOT NULL,
	caller_qualifier TEXT NOT NULL DEFAULT 'TOKEN',
	caller_host TEXT NOT NULL,
	host TEXT NOT NULL,
	service_class TEXT NOT NULL,
	service_name TEXT NOT NULL,
	called_method TEXT NOT NULL,
	duration NUMERIC NOT NULL,
	max_invocation_time NUMERIC NOT NULL,
	min_invocation_time NUMERIC NOT NULL,
	operation_count INTEGER NOT NULL DEFAULT 1,
	aggregated BOOLEAN NOT NULL DEFAULT true,
	start_time TIMESTAMP WITH TIME ZONE NOT NULL,
	end_time TIMESTAMP WITH TIME ZONE NOT NULL
);
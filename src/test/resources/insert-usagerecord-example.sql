INSERT INTO jobusagerecord 
(aggregated,caller_qualifier,consumer_id,creation_time,
duration,end_time,host,id,
job_name,max_invocation_time,min_invocation_time,operation_count,
operation_result,scope,service_class,service_name,
start_time) 
VALUES 
('true','TestCallerQualifier','name.surname','2021-03-15 12:20:32.705 +0100',
295,'2021-03-15 12:20:32.704 +0100','localhost','17abc69a-491e-47ed-994b-9e1db0cf05fd',
'TestJobName',295,295,1,
'SUCCESS','/gcube/devNext','TestServiceClass','TestServiceName',
'2021-03-15 12:20:32.704 +0100');

----------------------------------------------------------------------------------------

INSERT INTO portletusagerecord 
(aggregated,consumer_id,creation_time,end_time,
id,operation_count,operation_id,operation_result,
portlet_id,scope,start_time) 
VALUES 
('true','name.surname','2021-03-15 12:20:32.706 +0100','2021-03-15 12:20:32.705 +0100',
'724aee1c-3f7c-40b9-bb27-40211a7b10ca',1,'TestPortletOperationID','SUCCESS',
'TestPortlet','/gcube/devNext','2021-03-15 12:20:32.705 +0100');

----------------------------------------------------------------------------------------

INSERT INTO serviceusagerecord
(aggregated,called_method,caller_host,caller_qualifier,
consumer_id,creation_time,duration,end_time,
host,id,max_invocation_time,min_invocation_time,operation_count,
operation_result,scope,service_class,service_name,
start_time) 
VALUES 
('true','TestCalledMethod','remotehost','TestCallerQualifier',
'name.surname','2021-03-15 12:20:32.987 +0100',361,'2021-03-15 12:20:32.706 +0100',
'localhost','c386cf48-b955-4683-a3da-372216e34eb3',361,361,
1,'SUCCESS','/gcube/devNext','TestServiceClass',
'TestServiceName','2021-03-15 12:20:32.706 +0100');

----------------------------------------------------------------------------------------

INSERT INTO storagestatusrecord 
(aggregated,consumer_id,creation_time,data_count,
data_service_class,data_service_id,data_service_name,data_type,
data_volume,end_time,id,operation_count,
operation_result,provider_id,scope,start_time) 
VALUES ('true','name.surname','2021-03-15 12:20:32.988 +0100',8117,
'dataServiceClass','dataServiceId','dataServiceName','STORAGE',
8086,'2021-03-15 12:20:32.988 +0100','21d68adf-b17f-4a19-85ac-c50ba854cf72',1,
'SUCCESS','testprotocol://providerURI','/gcube/devNext','2021-03-15 12:20:32.988 +0100');

----------------------------------------------------------------------------------------

INSERT INTO storageusagerecord 
(aggregated,consumer_id,creation_time,data_type,
data_volume,end_time,id,operation_count,
operation_result,operation_type,provider_uri,resource_owner,
resource_scope,resource_uri,scope,start_time) 
VALUES 
('true','name.surname','2021-03-15 12:20:32.990 +0100','STORAGE',
9177,'2021-03-15 12:20:32.989 +0100','11c0f9bb-e04f-4035-a230-50160dad737e',1,
'SUCCESS','READ','testprotocol://providerURI','resource.owner',
'/infrastructure/vo','testprotocol://objectURI','/gcube/devNext','2021-03-15 12:20:32.989 +0100');


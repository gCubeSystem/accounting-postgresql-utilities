alter table storage_usage_record alter column resource_scope drop not null;
alter table storage_usage_record alter column resource_scope set default 'NOT_NEEDED';
alter table storage_usage_record alter column resource_uri drop not null;
alter table storage_usage_record alter column resource_uri set default 'NOT_NEEDED';

#update storage_usage_record set resource_scope='NOT_NEEDED', resource_uri='NOT_NEEDED';


alter table storage_status_record alter column data_service_id drop not null;
alter table storage_status_record alter column data_service_id set default 'NOT_NEEDED';
alter table storage_status_record alter column data_service_class drop not null;
alter table storage_status_record alter column data_service_class set default 'NOT_NEEDED';
alter table storage_status_record alter column data_service_name drop not null;
alter table storage_status_record alter column data_service_name set default 'NOT_NEEDED';

#update storage_status_record set data_service_id='NOT_NEEDED', data_service_class='NOT_NEEDED', data_service_name='NOT_NEEDED';



alter table storage_status_record ADD COLUMN provider_uri TEXT DEFAULT 'data.d4science.org';
UPDATE storage_status_record SET provider_uri='data.d4science.org', provider_id='data.d4science.org';


ALTER TYPE data_type ADD VALUE 'JUPYTER';
ALTER TYPE data_type ADD VALUE 'KUBERNETES';
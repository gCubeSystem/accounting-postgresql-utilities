package org.gcube.accounting.utility.postgresql;

import org.gcube.accounting.datamodel.UsageRecord;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostgreSQLQueryTest extends ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(PostgreSQLQueryTest.class);
	
	@Test
	public void testSQLStatementString() throws Exception {
		
		UsageRecord usageRecord = TestUsageRecord.getTestAggregatedJobUsageRecord();
		PostgreSQLQuery postgreSQLQuery = new PostgreSQLQuery();
		String sql = postgreSQLQuery.getSQLInsertCommand(usageRecord);
		logger.debug(sql);
		
		usageRecord = TestUsageRecord.getTestAggregatedPortletUsageRecord();
		sql = postgreSQLQuery.getSQLInsertCommand(usageRecord);
		logger.debug(sql);
		
		usageRecord = TestUsageRecord.getTestAggregatedServiceUsageRecord();
		sql = postgreSQLQuery.getSQLInsertCommand(usageRecord);
		logger.debug(sql);
		
		usageRecord = TestUsageRecord.getTestAggregatedStorageStatusRecord();
		sql = postgreSQLQuery.getSQLInsertCommand(usageRecord);
		logger.debug(sql);
		
		usageRecord = TestUsageRecord.getTestAggregatedStorageUsageRecord();
		sql = postgreSQLQuery.getSQLInsertCommand(usageRecord);
		logger.debug(sql);
	}
	
}

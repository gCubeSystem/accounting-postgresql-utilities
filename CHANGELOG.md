This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Accounting Postgresql Utilities

## [v2.0.0]

- RecordToDBMapping is no more singleton 


## [v1.0.0]

- First Release
